CREATE DATABASE
IF NOT TaniaRascia;

use TaniaRascia;

DROP TABLE IF EXISTS Users;

CREATE TABLE Users
(
	Id INT(11)
	UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	FirstName NVARCHAR
	(30) NOT NULL,
	LastName NVARCHAR
	(30) NOT NULL,
	email NphpVARCHAR
	(50) NOT NULL,
	age INT
	(3),
	location VARCHAR
	(50),


	`Date` TIMESTAMP --` is om keywords te escapen voor SQL
);